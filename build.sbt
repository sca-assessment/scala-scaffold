name := "ScaffoldProject"

version := "0.1"

// Use an environment variable for Scala version, defaults to 2.13.8
scalaVersion := sys.env.getOrElse("SCALA_VERSION", "2.13.8")

// Known bad package with vulnerabilities
libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value

// Known good package
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.8.1"